#!/usr/bin/env python

# Does the generator pause before reaching a yield or after?
# Before. But iterator.send() also causes the iterator to move and then stop before the next yield.
# So on the first next(), the generator stops a the first iteration and index is still 0.
# On the send(), the generator sets jump to 2 and then moves on till the end of the loop (thus index will be 2), then it pauses again at the next yield.
# Subsequent next() will set jump to None, increment the index, and stop at the next iteration.

# Calling send(None) in this case would be equivalent to next.

def jumping_range(up_to):
    """Generator for the sequence of integers from 0 to up_to, exclusive.

    Sending a value into the generator will shift the sequence by that amount.
    """
    index = 0
    while index < up_to:
        jump = yield index
        print("yoo-hoo")
        if jump is None:
            jump = 1
        index += jump
        print(index)

it = jumping_range(5)
next(it)
a = it.send(2)
print(a)
# next(it)

        