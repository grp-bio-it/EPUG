#!/usr/bin/env python

import asyncio

async def f(x):
    print(f"f({x}) gains control")
    print(f"f({x}) gives control to the event loop")
    await asyncio.sleep(1)
    print(f"The external task is done, f({x}) takes back control")
    print(f"f({x}) returns")
    return

async def main():
    await f("meow")
    await asyncio.gather(f("bau"), f("mooo"))
    print("main takes control")

asyncio.run(main())
