def process_image(image, preprocessors, postprocessors):
    """
    Args:
        image: A byte array where the last axis represents channels...
        preprocessors: An iterable of functions that must take images
                      as inputs and output other images
        postprocessors: An iterable of functions that take images as
                        input and output other images
    Returns:
        An instance of Image
    Raises:
        ValueError if image has more than 17 channels
    """
    ... # do something

all_that_effort_just_wasted = process_image(123, 456, 678)

#######################################

# from typing import Callable, Iterable
# class Image:
#     ...

# def process_image2(
#     image: Image,
#     preprocessors: Iterable[Callable[[Image], Image]],
#     postprocessors: Iterable[Callable[[Image], Image]],
# ) -> Image:
#     ...
#     return Image()

# not_on_my_watch = process_image2(123, 456, 678)