def compute_square_root(value: float) -> float:
    import math
    return math.sqrt(value)


# """WELL, CAN I DO THIS, THEN?"""

# result = compute_square_root(-1)

# """
# SO I GUESS YOU DON'T TAKE JUST *ANY* FLOAT...

# THE MORE PRECISE YOU CAN BE WITH YOUR TYPE HINTS, THE MORE
# TROUBLE YOU'LL AVOID IN THE FUTURE
# """

# "- THE TYPE CHECKER COULDN'T CATCH THIS ERROR"
# "  BECAUSE OF EXCEPTIONS. WE CAN TALK ABOUT THAT"
# "  LATER, BUT IT'S A LIMITATION OF PYTHON ITSELF"