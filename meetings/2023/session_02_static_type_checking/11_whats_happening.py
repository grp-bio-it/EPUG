"""
█░█░█ █░█ ▄▀█ ▀█▀ ▀ █▀   █░█ ▄▀█ █▀█ █▀█ █▀▀ █▄░█ █ █▄░█ █▀▀ ▀█
▀▄▀▄▀ █▀█ █▀█ ░█░ ░ ▄█   █▀█ █▀█ █▀▀ █▀▀ ██▄ █░▀█ █ █░▀█ █▄█ ░▄
"""

# "- A STATIC TYPE CHECKER VERIFIES **ALL** YOUR CODE;"
# "    - ALL YOUR FILES AND THEIR INTERACTIONS"

# "- CHECKS THAT THE ROUND PEGS GO IN THE ROUND HOLES"
# "    - i.e.: THE RIGHT ARGS GO IN THE RIGHT FUNCTIONS;"

# "    - AND THAT FUNCTIONS KEEP THEIR PROMISES"
# "        - i.e.: THEY RETURN WHAT THEY SHOULD"

# "- AMORPHOUS CLAY OF CODE -> LEGO BRICKS"