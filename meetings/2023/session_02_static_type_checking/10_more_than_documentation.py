"""
█▄░█ █▀█ ▀█▀   ░░█ █░█ █▀ ▀█▀   █▀▀ ▄▀█ █▄░█ █▀▀ █▄█
█░▀█ █▄█ ░█░   █▄█ █▄█ ▄█ ░█░   █▀░ █▀█ █░▀█ █▄▄ ░█░

█▀▀ █▀█ █▀▄▀█ █▀▄▀█ █▀▀ █▄░█ ▀█▀ █▀
█▄▄ █▄█ █░▀░█ █░▀░█ ██▄ █░▀█ ░█░ ▄█

Sure, type hints improve readbility of your code,
but there is so much more.
"""

def compute_square_root(value: float) -> float:
    import math
    return math.sqrt(value) # try vandalizing this