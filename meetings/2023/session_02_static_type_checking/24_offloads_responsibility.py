"""
█▀█ █▀▀ █▀▀ █░░ █▀█ ▄▀█ █▀▄
█▄█ █▀░ █▀░ █▄▄ █▄█ █▀█ █▄▀

█▀█ █▀▀ █▀ █▀█ █▀█ █▄░█ █▀ █ █▄▄ █ █░░ █ ▀█▀ █▄█
█▀▄ ██▄ ▄█ █▀▀ █▄█ █░▀█ ▄█ █ █▄█ █ █▄▄ █ ░█░ ░█░
"""

"- You can offload responsibility onto the type checker!"
"    - If you can express your requirements as types,"
"      then you never have to manually check that again"

# class Image:
#     ...

# class ImageProcessor:
#     def __init__(self) -> None:
#         self.intensity = None
#         self.blurryness = None
#         super().__init__()

#     def set_intensity(self, intensity: float):
#         self.intensity = intensity

#     def set_blurryness(self, blurryness: float):
#         self.blurryness = blurryness

#     def process(self, image: Image) -> Image:
#         if self.blurryness is None:
#             raise ValueError("Forgot to set blurryness")
#         if self.intensity is None:
#             raise ValueError("Forgot to set intensity")
#         ...
#         return Image()

# processed = ImageProcessor().process(Image())

# class ImageProcessor2:
#     def __init__(self, intensity: float, blurryness: float) -> None:
#         self.intensity = intensity
#         self.blurryness = blurryness
#         super().__init__()

#     def process(self, image: Image) -> Image:
#         ...
#         return Image()

# processed2 = ImageProcessor2().process(Image())
