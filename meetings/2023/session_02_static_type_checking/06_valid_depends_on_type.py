"""
█░█░█ █░█ ▄▀█ ▀█▀  █ █▀  █░█ ▄▀█ █░░ █ █▀▄
▀▄▀▄▀ █▀█ █▀█ ░█░  █ ▄█  ▀▄▀ █▀█ █▄▄ █ █▄▀

█▀▄ █▀▀ █▀█ █▀▀ █▄░█ █▀▄ █▀  █▀█ █▄░█  ▀█▀ █▄█ █▀█ █▀▀ █▀
█▄▀ ██▄ █▀▀ ██▄ █░▀█ █▄▀ ▄█  █▄█ █░▀█  ░█░ ░█░ █▀▀ ██▄ ▄█
"""


from typing import List

class Penguin:
    def swim(self):
        ...

some_int: int = 123
some_float: float = 3.14
some_string: str = "hello"
a_list_of_strings: List[str] = ["a", "b", "c"]
a_penguin: Penguin = Penguin()

"""
    YOU CAN OR CAN'T DO THINGS DEPENDING ON THE TYPES
    OF YOUR VARIABLES
"""

some_int += 17 # YOU CAN ADD NUMBER TO INTs
some_int.swim() # BUT YOU CAN'T ASK AN INT TO SWIM
a_penguin.swim() # PENGUINS CAN SWIM JUST FINE