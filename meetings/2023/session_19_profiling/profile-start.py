#!/usr/bin/env python3

import argparse
import random
import time

import numpy as np


def idle_nosort(input):
    time.sleep(1)
    return input


def thrashy_sort(input):
    # Function to find the partition position
    def partition(array, low, high):
        # choose the rightmost element as pivot
        pivot = array[high]

        # pointer for greater element
        i = low - 1

        # traverse through all elements
        # compare each element with pivot
        for j in range(low, high):
            if array[j] <= pivot:
                # If element smaller than pivot is found
                # swap it with the greater element pointed by i
                i = i + 1

                # Swapping element at i with element at j
                (array[i], array[j]) = (array[j], array[i])

        # Swap the pivot element with the greater element specified by i
        (array[i + 1], array[high]) = (array[high], array[i + 1])

        # Return the position from where partition is done
        return i + 1

    # function to perform quicksort

    def quickSort(array, low=0, high=None):
        if high is None:
            high = len(array) - 1

        if low < high:
            # Find pivot element such that
            # element smaller than pivot are on the left
            # element greater than pivot are on the right
            pi = partition(array, low, high)

            # Recursive call on the left of pivot
            quickSort(array, low, pi - 1)

            # Recursive call on the right of pivot
            quickSort(array, pi + 1, high)

    quickSort(input)
    return input


def efficient_sort(input):
    input.sort()
    return input


def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-e", "--efficient", action="store_true")
    group.add_argument("-q", "--slow", action="store_true")
    group.add_argument("-i", "--idle", action="store_true")
    arg = parser.parse_args()

    # Controls if we execute the succint or verbose code path
    brief = False

    low, high = 1, 500000
    basefactor = 50000
    factorA = basefactor
    factorB = 2 * basefactor
    factorC = 8 * basefactor
    factorD = 20 * basefactor

    def init(fact):
        return [random.randint(low, high) for i in range(fact)]

    if brief:
        sampleA, sampleB, sampleC = map(init, (factorA, factorB, factorC))
    else:
        sampleA = init(factorA)
        sampleB = init(factorB)
        sampleC = init(factorC)

    sampleD = np.random.randint(low, high, factorD) + 1

    if brief:
        samples = (sampleA, sampleB, sampleC, sampleD)

    if arg.efficient:
        if brief:
            sampleA, sampleB, sampleC, sampleD = map(efficient_sort, samples)
        else:
            sampleA = efficient_sort(sampleA)
            sampleB = efficient_sort(sampleB)
            sampleC = efficient_sort(sampleC)
            sampleD = efficient_sort(sampleD)
    elif arg.slow:
        if brief:
            sampleA, sampleB, sampleC, sampleD = map(thrashy_sort, samples)
        else:
            sampleA = thrashy_sort(sampleA)
            sampleB = thrashy_sort(sampleB)
            sampleC = thrashy_sort(sampleC)
            sampleD = thrashy_sort(sampleD)
    elif arg.idle:
        if brief:
            sampleA, sampleB, sampleC, sampleD = map(idle_nosort, samples)
        else:
            sampleA = idle_nosort(sampleA)
            sampleB = idle_nosort(sampleB)
            sampleC = idle_nosort(sampleC)
            sampleD = idle_nosort(sampleD)
    else:
        print("No such algorithm")


if __name__ == "__main__":
    main()
