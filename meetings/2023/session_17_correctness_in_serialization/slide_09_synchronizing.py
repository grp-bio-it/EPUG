# pyright: strict
from dataclasses import dataclass
import threading

from slides.slide_06_json_types import JsonObject, JsonValue

# We'll use "Message" types that only contain
# fields that MUST be serialized

@dataclass
class PersonMessage:
    name: str
    age: int

    def to_json(self) -> JsonObject:
        raise NotImplemented # assume magic for now

    @classmethod
    def from_json(cls, value: JsonValue) -> JsonObject:
        raise NotImplemented # assume magic for now

@dataclass
class Person:
    name: str
    age: int
    lock: threading.Lock # non-serializable field

    def to_message(self) -> PersonMessage:
        # can't forget to serialize any of the fields from PersonMessage
        return PersonMessage(name=self.name, age=self.age)

    @classmethod
    def from_message(cls, data: PersonMessage) -> "Person":
        # PersonMessage MUST have enough information to make the constructor happy
        return Person(name=data.name, age=data.age, lock=threading.Lock())

# Insight: Serializing to a "Message" type can help keep
#          serialization and deserialization in sync