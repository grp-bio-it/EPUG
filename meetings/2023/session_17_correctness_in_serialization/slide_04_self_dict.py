# pyright: strict
import json
from dataclasses import dataclass

@dataclass
class Person:
    name: str
    age: int

    def to_json(self) -> str:
        # Now we can't forget any field!
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, data: str) -> "Person":
        parsed = json.loads(data)
        # Hopefully this looks weird to everyone
        keys = Person("dummy", 0).__dict__.keys()
        return Person(**{
            key: parsed[key] for key in  keys
        })

# but this works!
bob = Person("Bob", 50)
serialized_bob = bob.to_json()
deserialized_bob = Person.from_json(serialized_bob)
assert deserialized_bob.name == "Bob"
assert deserialized_bob.age == 50
