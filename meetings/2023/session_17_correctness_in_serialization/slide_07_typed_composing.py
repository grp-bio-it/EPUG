# pyright: strict
from dataclasses import dataclass
from collections.abc import Mapping
from slide_06_json_types import JsonObject, JsonValue

@dataclass
class Color:
    hex_code: str

    def to_json(self) -> JsonObject:
        return self.__dict__

    @classmethod
    def from_json(cls, data: JsonValue) -> "Color":
        # if not isinstance(data, Mapping):        # uncomment to fix
        #     raise ValueError("expecting object") # uncomment to fix
        hex_code = data["hex_code"]
        # if not isinstance(hex_code, str):        # uncomment to fix
        #     raise ValueError("Expected string")  # uncomment to fix
        return Color(hex_code=hex_code)

@dataclass
class Person:
    name: str
    fav_color: Color

    def to_json(self) -> JsonObject:
        return {
            "name": self.name,
            "fav_color": self.fav_color.to_json()
        }

    @classmethod
    def from_json(cls, data: JsonValue) -> "Person":
        # if not isinstance(data, Mapping):        # uncomment to fix
        #     raise ValueError("Expected mapping") # uncomment to fix
        name = data["name"]
        # if not isinstance(name, str):           # uncomment to fix
        #     raise ValueError("Expected string") # uncomment to fix
        fav_color = Color.from_json(data["fav_color"]) # Ok, NOW it's fixed
        return Person(name=name, fav_color=fav_color)

ser_person = Person("Bob", fav_color=Color("#ffffff")).to_json()
person = Person.from_json(ser_person)