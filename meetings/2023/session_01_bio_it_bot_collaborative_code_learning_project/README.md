# Introducing the Bio-IT bot

The [Bio-IT bot](https://git.embl.de/grp-bio-it/bio-it-bot/) is a Mattermost bot
available on the [EMBL Chat](https://chat.embl.org)
under the name [BIT or bot-bio-it](https://chat.embl.org/embl/messages/@bot-bio-it).

The [Bio-IT bot](https://git.embl.de/grp-bio-it/bio-it-bot/) is a [Bio-IT community](https://bio-it.embl.de) developed project.
Most of its features have been developed by members of the [Bio-IT community](https://bio-it.embl.de).

As a community project, contributions to enhance its functionality or documentation are welcome from everyone.
You are encouraged to clone or fork it,
run it locally following its [getting started documentation](https://grp-bio-it.embl-community.io/bio-it-bot/bot.html),
tinker with it,
and open merge requests with any kind of contributions.

If you ever wanted to improve your Python knowledge, this project is an excellent playground as it is reviewed by different people.
Your will be able to learn about different programming concepts, such as:

- Database interaction, creation and manipulation (`PostgreSQL`)
- Authentication systems (`LDAP`,...)
- API interaction (`REST` and interfaces such as GitLab and OpenAI)
- RSS Feed use
- Asynchronous programming (`asyncio` and built-in Python libraries)
- Container technologies (`docker-compose`)
- Real-time data processing (`trivia` plugin)
- and many of the [libraries used in this project](https://git.embl.de/grp-bio-it/bio-it-bot/-/blob/main/utils/requirements.txt).

You will also learn from the code created by others,
and have your contributions reviewed by myself ([Renato Alves](https://www.embl.org/internal-information/people/renato-alves/))
and other contributors to the project.

The bot is designed around the [`mmpy_bot`](https://github.com/attzonko/mmpy_bot) framework
and uses the [`mattermostautodriver`](https://github.com/embl-bio-it/python-mattermost-autodriver)
interface, documented [here](https://embl-bio-it.github.io/python-mattermost-autodriver/),
to interact with [EMBL's Mattermost chat](https://chat.embl.org).

Contributions that enhance the functionality of the bot can be done in the form of [`mmpy_bot` plugins](https://mmpy-bot.readthedocs.io/en/stable/plugins.html).
Additional forms of contribution include:

- [Pytest and UnitTest automatic tests](https://git.embl.de/grp-bio-it/bio-it-bot/-/tree/main/tests)
- [Sphinx documentation improvements](https://git.embl.de/grp-bio-it/bio-it-bot/-/tree/main/docs)
- [Alembic/SQLalchemy database migrations](https://git.embl.de/grp-bio-it/bio-it-bot/-/tree/main/alembic) (for new plugins that need to store their own data)

As you can see there's a lot you can learn and explore, so don't hesitate to reach out.

For additional questions email [bio-it@embl.de](mailto:bio-it@embl.de)