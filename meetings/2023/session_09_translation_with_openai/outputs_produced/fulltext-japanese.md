# Pythonのチュートリアル

Pythonは学習しやすく、強力なプログラミング言語です。効率的な高水準データ構造と、オブジェクト指向プログラミングに対するシンプルで効果的なアプローチを持っています。Pythonのエレガントな構文と動的型付け、解釈型言語であることから、多くの分野でスクリプティングや迅速なアプリケーション開発に理想的な言語です。

Pythonインタプリタと広範な標準ライブラリは、Pythonのウェブサイト[https://www.python.org/](https://www.python.org/)からすべての主要プラットフォーム用のソースまたはバイナリ形式で自由に利用可能で、自由に配布することができます。同じサイトには、多くの無料のサードパーティPythonモジュール、プログラム、ツール、追加ドキュメントの配布やポインタも含まれています。

Pythonインタプリタは、CまたはC++（またはCから呼び出すことができる他の言語）で実装された新しい関数やデータ型を容易に拡張することができます。Pythonは、カスタマイズ可能なアプリケーションの拡張言語としても適しています。

このチュートリアルは、Python言語とシステムの基本的な概念と特徴を非公式に紹介します。実践的な経験のためにPythonインタプリタを手元に用意しておくと便利ですが、すべての例は自己完結型であり、オフラインでもチュートリアルを読むことができます。

標準オブジェクトとモジュールの説明については、[The Python Standard Library](https://docs.python.org/3/library/index.html#library-index)を参照してください。[The Python Language Reference](https://docs.python.org/3/reference/index.html#reference-index)は、言語のより形式的な定義を示します。CまたはC++での拡張を書く場合は、[Extending and Embedding the Python Interpreter](https://docs.python.org/3/extending/index.html#extending-index)と[Python/C API Reference Manual](https://docs.python.org/3/c-api/index.html#c-api-index)を読んでください。Pythonについて詳しく説明した書籍もあります。

このチュートリアルは、すべての機能や、よく使われる機能について網羅することを試みていません。代わりに、Pythonの最も注目すべき機能の多くを紹介し、言語の風味やスタイルを理解するのに良いアイデアを提供します。読み終えた後は、Pythonモジュールやプログラムを読み書きすることができ、[The Python Standard Library](https://docs.python.org/3/library/index.html#library-index)で説明されているさまざまなPythonライブラリモジュールについてもっと学ぶ準備ができるでしょう。