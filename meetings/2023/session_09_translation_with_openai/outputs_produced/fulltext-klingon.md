# The Python Tutorial

                  PythonʼraD loʼlaHbeʼchugh, wej loSchoH Huch Dujchuʼmaj. ʼOploʼ programming qIjDaj je taʼDIch hoch DatIHnISbeʼ, `object-oriented programming` qab choqej. Pythonʼchugh loQ simpawmoʼchuqpeH, `syntax` qIj ben vItlhutlh. Qapla' batlh bIngDaq pythonʼraD, Hoch ʼaDIʼ Daq ghojwIʼpuʼ wovquʼ jen SIQDIʼ, chenmoH ʼej most platforms.

                Python ʼItlh Qujmey qabluʼpuʼ, yuDajDaq naDev vayʼ Dun boqʼoy.  Qapla' batlh bIngDaq python web site [https://www.python.org/](https://www.python.org/) HochmoHpuʼ. Qujmey chenmoH ʼej  vIquv free third party python HoS ʼuʼ, ngeD ʼej vIloʼ documentation.

                Python ʼopmoHtaʼ vIrenpuʼ, qoj DIvIʼ, ChabalmoHvam yIloʼ, vaj chenmoHlang jayʼ. PythonʼraD chenmoHtaH,  C naD C++ puS chaʼmoHtaH vItlhutlh Duj. DaH chenmoH rur extensions vegh customizable  applications.

                  tutorial  chaʼlogh vIneHmoHDIʼ ʼIw viewqu' qap. Python wa'natlh DIvIʼ DIYtaHbeʼchugh, SuvwIʼmey vIghItlh. Tutorial vItlhutlh jImej Daq, quv, wej examples vIneHmoHDIʼ ʼej. Potlh tutorial wa'natlh loQDIʼ offline HIq.

                   [The Python Standard Library](https://docs.python.org/3/library/index.html#library-index) je ʼoploʼDaj wej chaʼloghvam.  [The Python Language Reference](https://docs.python.org/3/reference/index.html#reference-index) legh nuq vIneH, Sovta' loʼwI'. C naD C++DIʼ extensions veghmoHpuʼ, [Extending and Embedding the Python Interpreter](https://docs.python.org/3/extending/index.html#extending-index) Huch [Python/C API Reference Manual](https://docs.python.org/3/c-api/index.html#c-api-index) legh vIneHmoH. Python chen wa'loghlIj vIneHmoH.

                  tutorial vaj Humanbe'wIʼ yIyajbeʼchugh 'ej  retlhbeʼchughbe', retlhbeʼchughvetlh  wej nuqneHmey majQaʼ vItlhutlh. tutorial vaj PythonʼraD najDIʼ tIch, ghajpuʼ ʼej yIbuSHaʼ ʼopmoHtaʼDIʼ. Sovta' chaʼlogh vIneH ʼej [The Python Standard Library](https://docs.python.org/3/library/index.html#library-index)DIʼ  ngughmey Python HoS nIvbogh wej vIneH.