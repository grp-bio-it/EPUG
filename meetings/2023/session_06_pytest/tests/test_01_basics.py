"""
    Pytest: the basics.
"""
import pytest


def str_to_int(raw_input: str) -> int:
    """
    Cast the passed string value to integer.

    NB: typically the function to test gets imported from external module:

    from lib_foo import str_to_int
    """
    if not isinstance(raw_input, str):
        raise ValueError('Expected a string value.')
    return int(raw_input)

# ---

def test_convert_str_to_int():
    """
    Test the happy path.
    """
    assert str_to_int('123') == 123, 'Unexpected return value.'


def test_validate_input_type():
    """
    Test the input type validation.
    """
    with pytest.raises(ValueError):
        str_to_int(123.45)
    # what can be improved here?
    with pytest.raises(ValueError):
        str_to_int('foo')
