"""
    Pytest: advanced voodoo.

    NB: inspect the `conftest.py` file!
"""
import logging
import pathlib


def set_key(redis_client, key: str | bytes, value: int) -> None:
    """
    Save integer value in the key-value cache.
    """
    assert isinstance(key, (str, bytes))
    assert isinstance(value, int)
    count = redis_client.set(key, value)
    logging.info('Value set.')

def save_stuff(path: str, stuff: str):
    """
    Write some data into a file.
    """
    with open(path, 'w', encoding='utf-8') as file:
        file.write(stuff)

# ---

def test_set_cache_key(test_redis, caplog):
    """
    Cache a valid value.
    NB: note the built-in `caplog` fixture.
    """
    with caplog.at_level(logging.INFO):
        set_key(test_redis, 'foo', 1984)

    # verify side effects    
    assert test_redis.get('foo') == b'1984'

    # inspect the log
    assert caplog.messages[0] == 'Value set.'


def test_static_key_present(test_redis, static_data):
    """
    Make sure the staic data is available.
    """
    key, value = static_data
    assert test_redis.get(key) == value


def test_write_file(tmpdir):
    """
    Test the file writer.

    NB: note the `tmpdir` fixture.
    """
    path = pathlib.Path(tmpdir) / 'test_data.txt'
    assert not path.exists()

    save_stuff(path, 'foobar')

    assert path.exists()
    assert path.read_text() == 'foobar'
