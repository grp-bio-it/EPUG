# Installing streamlit

```
python3 -m venv venv
source venv/bin/activate
pip install streamlit
```

or the equivalent process in a conda environment.

# Running streamlit

```
streamlit run myscript.py
```

or to run the example included in this repository

```
streamlit run demo.py
```
