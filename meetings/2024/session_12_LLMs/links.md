LLMs for programming links
==========================

History of chatbots
-------------------
[ELIZA](https://en.wikipedia.org/wiki/ELIZA)

Web-based models
----------------
[Openai GPT4-4o](Openai GPT4-4o)

Local Models
------------

[GPT4All](https://www.nomic.ai/gpt4all)
[Ollama](https://ollama.com/)

Models used:

* llama3 from meta
* gwen2 from alibaba
* gemma2 from google

Plugin for code completion in visual studio
-------------------------------------------

ollama autocoder

models suggested in the chat
----------------------------

[github copilot](https://github.com/features/copilot)
[Mistral](https://chat.mistral.ai)
[Claude 3.5 Sonnet](https://claude.ai/onboarding)

RAG (thx Thomas)
----------------
[Langchain tutorial](https://github.com/langchain-ai/rag-from-scratch/blob/main/rag_from_scratch_1_to_4.ipynb)

Readings
--------

[iX 07/24](https://www.heise.de/select/ix/2024/7)

open questions
--------------

resources for running local llms at embl
