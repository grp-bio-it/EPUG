"""user greeter

Given a name, this script prints a simple greeting message: Hi "name". 
"""

import argparse

def hi(name):
    """informal greeting message

    Parameters
    ----------
    name: str
          Name to greet
    
    Returns
    -------
    str
        greeting message Hi ``name``
    """
    return f"Hi {name}"

def main():
    parser = argparse.ArgumentParser(description=hi.__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--name', dest = 'name', type = str, help="Name to greet", required=True)
    args = parser.parse_args()
    print(hi(args.name))

if __name__ == "__main__":
    main()