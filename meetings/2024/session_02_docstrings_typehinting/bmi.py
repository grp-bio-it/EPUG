def compute_bmi(name:str, age:int, city:str, height:float, weight:float) -> str:
    """
    Calculate BMI

    Args:
        name (str): The name of the person.
        age (int): The age of the person.
        city (str): The city where the person lives.
        height (float): The height of the person in centimeters.
        weight (float): The weight of the person in kilograms.

    Returns:
        str: A formatted string containing information about the person.
    """
    bmi:float = weight / ((height / 100) ** 2)
    # Construct a string with information about the person
    result_str = (
        f"Name: {name}\n"
        f"Age: {age} years\n"
        f"City: {city}\n"
        f"Height: {height} cm\n"
        f"Weight: {weight} kg\n"
        f"BMI: {bmi:.2f}"
    )

    return result_str

print(compute_bmi("Batman", 48.0, "Gotham", 170, 95))
