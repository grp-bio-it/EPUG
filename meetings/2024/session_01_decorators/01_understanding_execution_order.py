print("> Before decorator is defined")


def decorate(func):
    print(">> Start of decorator")

    def inner(*args, **kwargs):

        print()
        print(">> Before calling")

        output = func(*args, **kwargs)

        print(">> After calling")
        print()

        return output

    print(">> End of decorator")
    return inner


print("> After decorator is defined")
print("> Before decorated function")


@decorate
def hello_world():
    print(">>> Hello World")
    return ">>> And returning an Hello"

# NOTE: the @decorate syntax above is equivalent to writing
#
#   def hello_world():
#       (...)
#
# followed by:
#
#   hello_world = decorate(hello_world)
#
# Note that the decorate() function receives a function object
# as its first argument without calling the hello_world function


print("> After decorated function")

print(hello_world())
