from functools import wraps

print("> Before decorator is defined")


def decorator_factory(modify_output=False):
    print(">> Beginning of decorator factory")

    def decorator(func):
        print(">> Beginning of decorator on function", func.__name__)

        @wraps(func)
        def wrapper(*args, **kwargs):
            print(f">> Beginning wrapper on {func.__name__} with modify_output = {modify_output}")
            output = func(*args, **kwargs)
            print(f">> Ending wrapper on {func.__name__} with modify_output = {modify_output}")
            if modify_output:
                return f">>>>> {output} <<<<<"
            else:
                return output

        print(">> End of decorator on function", func.__name__)
        return wrapper

    print(">> End of decorator factory")
    return decorator


print("> After decorator is defined")
print("> Before decorated function - hello_world")


# Notice that when using a decorator factory, we need to call () the
# decorator factory and pass any relevant arguments to it
@decorator_factory()
def hello_world():
    """Returns Hello world."""

    print(">>> In function hello_world")
    return "🫶 Hello world"


print("> After decorated function - hello_world")
print("> Before decorated function - stylize_hello_world")


@decorator_factory(modify_output=True)
def stylize_hello_world():
    """Stylized Hello world."""

    print(">>> In function stylize_hello_world")
    return "🫶 Hello world"


print("> After decorated function - stylize_hello_world")

print("> Calling hello_world")
print(hello_world())
print("> Done calling hello_world")
print("> Calling stylize_hello_world")
print(stylize_hello_world())
print("> Done calling stylize_hello_world")
