from functools import wraps


def decorate(func):
    # wraps ensures docstrings and function signature are transferred
    # from the original function to the wrapper to ensure help()
    # and code inspection tools behave transparently
    @wraps(func)
    def wrapper(*args, **kwargs):
        # args[0] will be our "name" argument that we are
        # passing untouched to func()
        # If we wanted to modify it the wrapper could be written as:
        #   def wrapper(name, *args, **kwargs)
        # but we need to make sure that we also pass it to func() later:
        #   func(name, *args, **kwargs)
        output = func(*args, **kwargs)
        # Modifies the original output
        return f"🎉 {output} 🎉"

    # The wrapper function is returned
    return wrapper


@decorate
def hello_world(name):
    """This function returns Hello World followed by a name"""

    return f"Hello world: {name}"


# Same as:
# hello_world = decorate(hello_world)

print(hello_world("Renato"))

# the above would have been equivalent to:
# result = decorate(hello_world)("Renato")
