from functools import wraps


def custom_decorator(style_char="🎉"):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            output = func(*args, **kwargs)
            return f"{style_char}{output}{style_char}"

        return wrapper
    return decorator


@custom_decorator("🎉")
def tada_text():
    """This function returns a celebratory sentence"""

    return "James Northwick - Eat, drink, and be happy for tomorrow is not certain"


@custom_decorator("💚")
def gratitude_text():
    """This function returns Hello World followed by a name"""

    return "Maya Angelou - This is a wonderful day. I have never seen this one before."


print(gratitude_text())
print(tada_text())
