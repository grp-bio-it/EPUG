#!/usr/bin/env python
"""
Fill the KAAS form with selenium
"""

import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager

kaas_url = "https://www.genome.jp/kaas-bin/kaas_main"
email_addr = "federico.marotta@embl.de"

# Takes care of installing chrome and its driver
service = Service(ChromeDriverManager().install())

# Set up browser-specific options
options = Options()
# options.add_experimental_option("detach", True)

# Initialize the driver
driver = webdriver.Chrome(
    service = service,
    options = options,
)

# If an element is not immediately present in the page, wait up to 10 seconds by default
driver.implicitly_wait(10)

# Get the KEGG page
driver.get(kaas_url)

# Check the GHOSTX radio button
ghostx_radio = driver.find_element(By.CSS_SELECTOR, "input[value='GHOSTX']")
print(ghostx_radio)
ghostx_radio.click()

# Fill the textbox with the sequence
sequence_textbox = driver.find_element(By.NAME, "text")
print(sequence_textbox)
print(sequence_textbox.text)
sequence_textbox.clear()
sequence_textbox.send_keys(">MySequence")
sequence_textbox.send_keys(Keys.ENTER)
sequence_textbox.send_keys("MLWRTTPAALGY")

# Fill the textbox with our email address
mail_textbox = driver.find_element(By.NAME, "mail")
mail_textbox.clear()
mail_textbox.send_keys(email_addr)

# Scroll to the bottom of the page (by executing some javascript)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

# Click the "representatives" button
representative_button = driver.find_element(By.XPATH, "//input[@value=' for Prokaryotes ']")
print(representative_button.text)
time.sleep(2)
representative_button.click()

# Click "submit"
submit_button = driver.find_element(By.XPATH, "//input[@value=' Compute ']")
print(submit_button.text)
time.sleep(2)
submit_button.click()

# Wait up to 1h for keyboard interrupt, then quit (we could also use the "detach" option...)
try:
    time.sleep(600)
except KeyboardInterrupt:
    print("Bye!")
finally:
    driver.quit()
