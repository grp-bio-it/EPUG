import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager

service = Service(ChromeDriverManager().install())

options = Options()
# options.add_experimental_option("detach", True)

driver = webdriver.Chrome(
    service = service,
    options = options,
)

driver.get("https://www.genome.jp/kaas-bin/kaas_main")
driver.implicitly_wait(5)

ghostx = driver.find_element(By.CSS_SELECTOR, "input[value='GHOSTX']")
print(ghostx)
ghostx.click()

textbox = driver.find_element(By.NAME, "text")
textbox.send_keys(">Seq1", Keys.ENTER, "GWART")

time.sleep(1)

mailbox = driver.find_element(By.NAME, "mail")
mailbox.clear()
mailbox.send_keys("federico.marotta@embl.de")

time.sleep(2)

driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

time.sleep(2)

compute_btn = driver.find_element(By.CSS_SELECTOR, "input[value=' Compute ']")
compute_btn.click()


time.sleep(60)
