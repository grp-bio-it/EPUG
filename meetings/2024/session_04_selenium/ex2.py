from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

service = Service(ChromeDriverManager().install())

options = Options()
options.add_experimental_option("detach", True)

driver = webdriver.Chrome(
    service = service,
    options = options,
)

driver.get("http://selenium.dev")

driver.save_screenshot("/tmp/selenium_screenshot.png")

driver.quit()
